# Ansible Role: Transmission

Install transmission daemon.

## Requirements

- cf ``meta/main.yml`` for OS version requirements

## Role Variables

Available variables are listed on `defaults/main.yml`

## Dependencies

None

## Example playbook

```yaml
- hosts: all
  roles:
    - role: barlebenx.common.transmission
```
