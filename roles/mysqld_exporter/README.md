# Ansible Role: mysqld_exporter

Install mysqld_exporter for prometheus metrics.

See https://github.com/prometheus/mysqld_exporter for more info.

## Requirements

- See meta for distribution requirements.

- Required Grants in mysql / mariadb server :

    ```sql
    CREATE USER 'mysqld_exporter'@'localhost' IDENTIFIED BY 'XXXXXXXX' WITH MAX_USER_CONNECTIONS 3;
    GRANT PROCESS, REPLICATION CLIENT, SELECT ON *.* TO 'mysqld_exporter'@'localhost';
    ```

## Role Variables

Check in `./defaults/main.yml`

## Dependencies

## Example Playbook

```yaml
- hosts: all
  roles:
    - barlebenx.common.mysqld_exporter
```

## License

MIT
