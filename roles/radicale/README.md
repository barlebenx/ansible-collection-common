# Ansible Role: Radicale

Manage radicale dav server.

## Requirements

- cd `meta/main.yml`

## Role Variables

Available variables are listed on `defaults/main.yml`

## Dependencies

None

## Example playbook

```yaml
- hosts: all
  vars:
    radicale_users:
      - name: bob
        password: bob
      - name: alice
        password: $apr1$mysalt$Au6.TISdpaC94hNzoR4MS/
        encrypted: true
  roles:
    - role: barlebenx.common.radicale
```
