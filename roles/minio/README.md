# Ansible Role: minio

Install MinIO server.

## Requirements

- See meta for distribution requirements.

## Role Variables

Check in `./defaults/main.yml`

## Dependencies

## Example Playbook

```yaml
- hosts: all
  roles:
    - barlebenx.common.minio
```

## License

MIT
