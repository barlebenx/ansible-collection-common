# Ansible Role: node_exporter

Install node_exporter for prometheus metrics.

## Requirements

See meta for distribution requirements

## Role Variables

Check in `./defaults/main.yml`

## Dependencies

## Example Playbook

```yaml
- hosts: all
  roles:
    - barlebenx.common.node_exporter
```

## License

MIT
