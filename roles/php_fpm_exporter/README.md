# Ansible Role: php_fpm_exporter

Install php_fpm_exporter for prometheus metrics.

https://github.com/bakins/php-fpm-exporter

## Requirements

See meta for distribution requirements

## Role Variables

Check in `./defaults/main.yml`

## Dependencies

## Example Playbook

```yaml
- hosts: all
  roles:
    - barlebenx.common.php_fpm_exporter
```

## License

MIT
