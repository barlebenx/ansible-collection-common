# Ansible Role: Fail2ban

Basic install of fail2ban on debian.

## Requirements

- debian 9 or 10

## Role Variables

| name                    | default                                                         | description                                                       |
|-------------------------|-----------------------------------------------------------------|-------------------------------------------------------------------|
| fail2ban_ignored_ips    | []                                                              |                                                                   |
| fail2ban_bantime        | 1296000                                                         | 15 days                                                           |
| fail2ban_findtime       | 86400                                                           | 1 day                                                             |
| fail2ban_maxretry       | 3                                                               |                                                                   |

## Dependencies

## Example playbook

```yaml
- hosts: all
  roles:
    - role: barlebenx.common.fail2ban
```
