# Ansible Role: ssh

Setup ssh service with sshd_config.

By default security requirements are apply.

## Requirements

- See meta for OS requirements

## Role Variables

Check in `./defaults/main.yml`

## Dependencies

## Example Playbook

```yaml
- hosts: all
  roles:
    - barlebenx.common.ssh
```
