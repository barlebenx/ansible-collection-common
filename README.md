# Ansible collection: barlebenx.common

[![pipeline status](https://gitlab.com/barlebenx/ansible-collection-common/badges/main/pipeline.svg)](https://gitlab.com/barlebenx/ansible-collection-common/-/commits/main)

## Roles

See the `README.md` in the roles folders : `roles/*/README.md`.

## Playbooks for k3s

Playbooks for install / remove [k3s](https://k3s.io/).

| Playbook      | Description |
|---------------|-------------|
| k3s_setup.yml | Install k3s |
| k3s_reset.yml | Reset k3s   |


### Inventory

The following ansible inventory is an example for running this playbook.  
Note that groups `k3s_cluster`, `k3s_master` and `k3s_agent` are used in playbooks.


```yaml
---
all:
  vars:
    k3s_extra_server_args: --flannel-iface eth1 --disable servicelb --disable traefik --disable local-storage
    k3s_extra_agent_args: --flannel-iface eth1
    k3s_kube_vip_iface: eth1
    k3s_api_vip: "192.168.57.30"
    k3s_kube_vip_install: true
  hosts:
    kmaster1:
      ansible_user: root
      ansible_host: "192.168.57.31"
    kmaster2:
      ansible_user: root
      ansible_host: "192.168.57.32"
    kmaster3:
      ansible_user: root
      ansible_host: "192.168.57.33"
    kworker1:
      ansible_user: root
      ansible_host: "192.168.57.41"
    kworker2:
      ansible_user: root
      ansible_host: "192.168.57.42"

  children:
    k3s_cluster:
      children:
        k3s_master:
          hosts:
            kmaster1:
            kmaster2:
            kmaster3:
        
        k3s_agent:
          hosts:
            kworker1:
            kworker2:
```
